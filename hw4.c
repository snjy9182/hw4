/*  File: hw4.c
    Homework 4, 600.120(04) Spring 2017
    Date: March 10, 2017
    Name: Sun Jay Yoo
    Login: syoo21
    Email: syoo21@jhu.edu
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "dnasearch.h"

int main(int argc, char* argv[]) {

	//Check if there are enough command line arguments.
    if (argc != 2) {
        puts("Please specify the command-line arguments for the input file.");
        return 1;
    }

    //Import the input file to read and the output file to write.
    FILE* finput = fopen(argv[1], "r");

	//Check that the input file is not empty.
    if(finput == NULL){
        puts("Error opening file.");
        return 1;
    }

    //Formats input dna string into proper format (no spaces, all caps) for analysis.
    char nucleotide[2];
	char* dna = (char*) malloc(15000 * sizeof(char)); 
	if (!dna) {
	    puts("malloc fail, aborting"); 
	    return -1; 
	}
    while (fscanf(finput, "%c", nucleotide) != EOF){
    	if (nucleotide[0] == '\n' || isspace(nucleotide[0])) 
    		continue;
    	if (nucleotide[0] > 90) 
    		nucleotide[0] -= 32;
    	if (nucleotide[0] == 'A' || nucleotide[0] == 'C' || nucleotide[0] == 'G' || nucleotide[0] == 'T'){
    		strcat(dna, &nucleotide[0]);
    	} else {
    		puts("Invalid text file");
    		return 1;
    	}
    }

    //Gathers pattern input from stdin.
	printf("Input DNA patterns to search (press enter/space after each input and type enter and Control-D to indicate end of file):\n");
	char** pattern = (char**) malloc(1000 * sizeof(char *));
	for(int i = 0; i < 1000; i ++){
    	pattern[i] = (char*) malloc(1000 * sizeof(char)); 
	}	
	int i = 0;
	
	while (scanf("%s",pattern[i])!=EOF){
		i++;
	}
	
	//Implements dnaSearch for the pattern in the dna. Prints appropriate results for every pattern.
	for (int pattern_index = 0; pattern_index < i ; pattern_index++){
      	printf("%s", pattern[pattern_index]);
      	int initial = dnaSearch(pattern[pattern_index], dna, 0);
      	if (initial == -1){
            printf(" Not found");
        } else {
        	for (int dna_index = 0; dna_index < (int)strlen(dna); dna_index++) { //change condition??
        		dna_index = dnaSearch(pattern[pattern_index], dna, dna_index);
        		if (dna_index == -1) {
        			break;
        		}
        		printf(" %d", dna_index);
        	}
        }
        printf("\n");
    }

/////////////////////////////////////////////////////////////
	return 0;
}

