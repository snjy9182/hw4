/*  File: unittest.c
    Homework 4, 600.120(04) Spring 2017
    Date: March 10, 2017
    Name: Sun Jay Yoo
    Login: syoo21
    Email: syoo21@jhu.edu
*/

#include <stdio.h>
#include "dnasearch.h"

// Return value is the first offset *greater than
//   or equal to* start_at where pattern occurs in text.
//   Returns -1 if there is no such occurrence.
int pattern_match(char t[], char p[], int start_at){
	return dnaSearch(p, t, start_at);
}

int main() {
	char dna[] = "CATTACCGAGGG";

	char test1[] = "TAC";
	int test_1 = pattern_match(dna, test1, 0);
	if (test_1 != 3){
		puts("Test 1 failed.");
	}

	char test2[] = "A";
	int test_2 = pattern_match(dna, test2, 0);
	if (test_2 != 1){
		puts("Test 2 failed.");
	}
	
	char test3[] = "A";
	int test_3 = pattern_match(dna, test3, 2);
	if (test_3 != 4){
		puts("Test 3 failed.");
	}

	char test4[] = "GG";
	int test_4 = pattern_match(dna, test4, 0);
	if (test_4 != 9){
		puts("Test 4 failed.");
	}

	char test5[] = "GG";
	int test_5 = pattern_match(dna, test5, 10);
	if (test_5 != 10){
		puts("Test 5 failed.");
	}

	char test6[] = "GGG";
	int test_6 = pattern_match(dna, test6, 0);
	if (test_6 != 9){
		puts("Test 6 failed.");
	}

	char test7[] = "CATTACC";
	int test_7 = pattern_match(dna, test7, 0);
	if (test_7 != 0){
		puts("Test 7 failed.");
	}
	
	if (test_1 == 3 && test_2 == 1 && test_3 == 4 && test_4 == 9 && test_5 == 10 && test_6 == 9 && test_7 == 0){
		puts("All tests passed.");
	}


  return 0;
}
