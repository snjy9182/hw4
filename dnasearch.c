/*  File: dnasearch.c
    Homework 4, 600.120(04) Spring 2017
    Date: March 10, 2017
    Name: Sun Jay Yoo
    Login: syoo21
    Email: syoo21@jhu.edu
*/

#include <stdio.h>
#include <string.h>
#include "dnasearch.h"

int dnaSearch (char* pattern, char* dna, int start_at){

    //Initializes lengths of the pattern and the dna string.
    int n = strlen(pattern);
    int m = strlen(dna);

    //Loops through every possible position in the dna and makes sure the first instance of the pattern from start_at is found.
    //Returns position of pattern, or -1 if not found.
    for (int i = start_at; i <= m - n; i++){
        int match = 1; // initialize as true
        for (int j = 0; j < n; j++){
            if (pattern[j] != dna[i+j]){
                match = 0;
                break;
            }
        }
        if (match) {
            return i;
        }
    }
    return -1;
}
